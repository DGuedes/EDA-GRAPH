#ifndef PriorityQueue
#include "priority_queue.h"
#endif

#include <iostream>

using namespace std;

void PriorityQueue::flush()
{
}

Way *PriorityQueue::return_best()
{
  if (this->get_length() > 0) {
    Way *store = this->get_first();
    int highest = store->get_weigth();
    Way *aux = this->get_first();
    for (unsigned int i=0; i < this->get_length(); i++) {
      if (aux->get_weigth() < highest) {
        store = aux;
        highest = store->get_weigth();
      }
      aux = aux->get_prev();
    }
    return store;
  } else {
    cout << "Empty way" << endl;
  }
}


void PriorityQueue::set_first(Way *new_first)
{
  this->first = new_first;
}

bool PriorityQueue::should_have(Way *example)
{
  if (this->get_length() > 0) {
    Way *aux = this->get_first();
    for (unsigned int i=0; i < this->get_length(); i++) {
      if (aux->get_destination()->get_name() == example->get_destination()->get_name() || aux->get_origin()->get_name() == example->get_destination()->get_name()) {
        if (example->get_weigth() > aux->get_weigth()) {
          return false;
        }
      }
      aux = aux->get_prev();
    }
    return true;
  }
}

Way* PriorityQueue::get_first()
{
  return this->first;
}

void PriorityQueue::set_last(Way *new_last)
{
  this->last = new_last;
}

Way *PriorityQueue::get_last()
{
  return this->last;
}

void PriorityQueue::set_length(int new_length)
{
  this->length = new_length;
}

int PriorityQueue::get_length()
{
  return this->length;
}

void PriorityQueue::push(Way *w)
{
  if (this->get_length() > 0) {
    this->get_last()->set_prev(w);
    w->set_next(this->get_last());
    this->increment_length();
    this->set_last(w);
  } else {
    this->set_last(w);
    this->set_first(w);
    this->set_length(1);
  }
}

void PriorityQueue::increment_length()
{
  this->set_length(this->get_length() + 1);
}

PriorityQueue::PriorityQueue()
{
  this->length = 0;
  this->last = NULL;
  this->first = NULL;
}

void PriorityQueue::set_found(Way *new_found)
{
  this->found = new_found;
}

Way *PriorityQueue::get_found()
{
  return this->found;
}

bool PriorityQueue::check_best(City *destination_c, int new_distance)
{
  Way *a = this->get_first();
  for (unsigned int i=0; i < this->get_length() - 1; i++) {
    if (a->get_destination()->get_name() == destination_c->get_name()) {
      if (a->get_weigth() > new_distance) {
        return true;
      }
    }
  }
  return false;
}

bool PriorityQueue::is_present(City *future_destination)
{
  if (this->get_length() > 0) {
    Way *aux = this->get_first();
    for (unsigned int i=0; i < this->get_length(); i++) {
      // cout << aux->get_destination()->get_name() << " = " << future_destination->get_name() << " ?" << endl;
      if (aux->get_destination()->get_name() == future_destination->get_name())
        return true;
      aux = aux->get_prev();
    }
    return false;
  } else {
    cout << "Empty Queue" << endl;
  }
}


void PriorityQueue::show()
{
  if (this->get_length() > 0) {
    Way *aux = this->get_first();
    for (unsigned int i=0; i < this->get_length() - 1; i++) {
      cout << aux->get_name() << " DISTANCIA: " << aux->get_weigth() << endl;
      aux = aux->get_prev();
    }
    cout << aux->get_name() << " DISTANCIA: " << aux->get_weigth() << endl;
  } else {
    cout << "Empty Way" << endl;
  }
}

void PriorityQueue::decrement_length()
{
  this->set_length(this->get_length() - 1);
}

void PriorityQueue::pop()
{
  if (this->get_length() > 0) {
    if (this->get_length() > 1) {
      cout << "ta aqui" << endl;
      Way *aux = this->get_first()->get_prev();
      Way *to_be_deleted = this->get_first();
      this->set_first(aux);
      this->decrement_length();
      cout << aux->get_name() << endl;
      aux->show_each_path();
    } else {
      this->first = NULL;
      this->last = NULL;
      this->set_length(0);
    }
  } else {
    cout << "Empty Way" << endl;
  }
}