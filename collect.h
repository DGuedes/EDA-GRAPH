class Collect
{
private:
  City *first;
  City *last;
  unsigned int length;
public:
  void set_first(City *new_first);
  City *get_first();

  void set_last(City *new_last);
  City *get_last();

  void set_length(unsigned int new_length);
  unsigned int get_length();

  void popule();
  void show();
  void increment_length();

  void push(City *c);
  void collect_city(City *c, char *abrev);
  Collect();
  ~Collect();
  void garbage_collector();
  void popule_path(City *c, char *abrev);
  void best_first_search(string const origin, string const destination);
  City *search_by_name(string const origin);
  void search_solution(City *c1, City *c2);
};