#include <iostream>

using namespace std;

#include "path.h"

Path::Path()
{
  this->distance = 0;
}

void Path::to_s()
{
  cout << "blabla" << endl;
}

Path::~Path()
{
}

Path::Path(Path *example)
{
  this->set_origin(example->get_origin());
  this->set_destination(example->get_destination());
  this->set_distance(example->get_distance());
  this->set_name(example->get_name());
  this->prev = example->prev;
  this->next = example->next;
}

void Path::set_origin(City *new_origin)
{
  this->origin = new_origin;
}

City *Path::get_origin()
{
  return this->origin;
}

void Path::set_destination(City *new_destination)
{
  this->destination = new_destination;
}

City *Path::get_destination()
{
  return this->destination;
}

void Path::set_name(string new_name)
{
  this->name = new_name;
}

string Path::get_name()
{
  return this->name;
}

void Path::set_distance(int new_distance)
{
  this->distance = new_distance;
}

int Path::get_distance()
{
  return this->distance;
}

void Path::set_next(Path *new_next)
{
  this->next = new_next;
}

Path *Path::get_next()
{
  return this->next;
}

void Path::set_prev(Path *new_prev)
{
  this->prev = new_prev;
}

Path *Path::get_prev()
{
  return this->prev;
}
