#ifndef Way
#include "way.h"
#endif

void Way::show_each_path()
{
  if (this->get_length() > 0) {
    cout << "WAY:" << endl;
    Path *aux = this->get_start();
    for (unsigned int i=0; i < this->get_length() - 1; i++) {
      cout << aux->get_origin()->get_name() << " -> " << aux->get_destination()->get_name() << endl;
      aux = aux->prev;
    }
    cout << aux->get_origin()->get_name() << " -> " << aux->get_destination()->get_name() << endl;
    cout << "way end" << endl;
  } else {
    cout << "empty way" << endl;
  }
}

Path *Way::get_clear_path(City *example)
{
  if (this->get_length() > 0) {
    Path *aux = example->get_first_possible_path();
    cout << "POSIBBLE PATHS: " << example->get_possible_paths() << endl;
    for (unsigned int i=0; i < example->get_possible_paths(); i++) {
      cout << aux->get_destination()->get_name() << " = " << this->get_finish()->get_origin()->get_name() << " ?" << endl;
      if (aux->get_destination()->get_name() != this->get_finish()->get_origin()->get_name()) {
        return aux;
      } else {
        aux = aux->prev;
      }
    }
    return NULL;
  } else {
    cout << "Empty way" << endl;
  }
  // Path *aux;
  // aux = example->get_first_possible_path();
  // while (this->has(aux->get_destination()->get_name()) && aux->prev != NULL) {
  //   aux = aux->prev;
  // }
  // if (aux != NULL) {
  //   cout << "CLEAR AUX:" << aux->get_origin()->get_name() << " -> " << aux->get_destination()->get_name() << endl;
  //   return aux;
  // } else {
  //   return NULL;
  // }
}

void Way::set_start(Path *new_start)
{
  this->start = new_start;
}

Path *Way::get_start()
{
  return this->start;
}

void Way::set_finish(Path *new_finish)
{
  this->finish = new_finish;
}

Path *Way::get_finish()
{
  return this->finish;
}

void Way::set_length(int new_length)
{
  this->length = new_length;
}

int Way::get_length()
{
  return this->length;
}

Way::Way(Way *to_copy)
{
  this->length = 0;
  this->weigth = 0;
  this->set_name("");
  if (to_copy->get_length() > 0) {
    Path *aux = to_copy->get_start();
    for (unsigned int i=0; i < to_copy->get_length() - 1; i++) {
      this->push(aux);
      aux = aux->prev;
    }
    this->push(aux);
  }
}

bool Way::has(string future_path)
{
  bool flag = false;
  Path *aux = this->get_start();
  if (this->get_length() > 0) {
    for (unsigned int i=0; i < this->get_length() - 1; i++) {
      if (aux->get_destination()->get_name() == future_path || aux->get_origin()->get_name() == future_path) {
        flag = true;
        break;
      }
      aux = aux->prev;
    }
  } else {
    std::cout << "Empty way" << endl;
  }
  return flag;
}

void Way::to_s()
{
  if (this->get_length() > 1) {
    Path *aux = this->get_start();
    for (unsigned int i=0; i < this->get_length(); i++) {
      aux->to_s();
    }
    aux->to_s();
    cout << endl;
  } else {
    cout << "Empty way" << endl;
  }
}

void Way::show()
{
  if (this->get_length() > 0) {
    Path *aux = this->get_start();
    cout << "START: " << aux->get_origin()->get_name() << " -> " << aux->get_destination()->get_name() << endl;
    for (unsigned int i=0; i < this->get_length()-1; i++) {
      std::cout << aux->get_name() << endl;
      aux = aux->prev;
      cout << aux->get_origin()->get_name() << " -> " << aux->get_destination()->get_name() << endl;
    }
  }
  else {
    std::cout << "Empty way" << endl;
  }
}

void Way::push(Path *p)
{
  cout << "LENGTH:" << this->get_length() << endl;
  if (this->get_length() > 0) {
    cout << "quebraaqui" << endl;
    if (this->get_finish() == NULL) {
      cout << "NULO" << endl;
    } else {
      cout << "NAO NULO" << endl;
    }
    this->get_finish()->set_prev(p);

    p->set_next(this->get_finish());

    this->set_finish(p);

    cout << this->get_finish()->get_origin()->get_name() << " -> " << this->get_finish()->get_destination()->get_name() << endl;

    this->length++;
    this->set_destination(this->get_finish()->get_destination());
    this->set_name(this->get_name() + " -> ");
    this->set_name(this->get_name() + this->get_finish()->get_destination()->get_name());
    this->set_weigth(this->get_weigth() + p->get_distance());
  } else {
    this->set_finish(p);
    this->set_start(p);
    this->set_length(1);
    this->set_origin(this->get_start()->get_origin());
    this->set_destination(this->get_start()->get_destination());
    this->set_weigth(p->get_distance());
    this->set_name(this->get_finish()->get_origin()->get_name() + " -> ");
    this->set_name(this->get_name() + this->get_finish()->get_destination()->get_name());
  }
}

void Way::set_weigth(int new_weigth)
{
  this->weigth = new_weigth;
}

int Way::get_weigth()
{
  return this->weigth;
}

Way::Way()
{
  this->weigth = 0;
  this->length = 0;
  this->set_name("*");
}

void Way::set_prev(Way *new_prev)
{
  this->prev = new_prev;
}

Way *Way::get_prev()
{
  return this->prev;
}

void Way::set_next(Way *new_next)
{
  this->next = new_next;
}

Way *Way::get_next()
{
  return this->next;
}

string Way::get_name()
{
  return this->name;
}

void Way::set_name(string new_name)
{
  this->name = new_name;
}

void Way::set_origin(City *new_origin)
{
  this->origin = new_origin;
}

City *Way::get_origin()
{
  return this->origin;
}

void Way::set_destination(City *new_destination)
{
  this->destination = new_destination;
}

City *Way::get_destination()
{
  return this->destination;
}