#ifndef Graph
class Graph
{
private:
  unsigned int length;
  City *first_city;
  City *last_city;
public:
  unsigned int get_length();
  void set_length(unsigned int new_length);

  Graph();
  ~Graph();

  void set_first_city(City *new_first_city);
  City *get_first_city();

  void set_last_city(City *new_last_city);
  City *get_last_city();

  void push_city(City *new_city);
  void increment_length();

  void fetch(City *c, const char *abrev);
  void get_fetch();

  void display_cities();
  void fetch_path(City *city, char const *abrev);

  City *seek_for_city(string const name);

};
#endif