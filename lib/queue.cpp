#ifndef Queue
#include "queue.h"
#endif

#include <iostream>

using namespace std;

Queue::Queue()
{
    cout << "Queue created." << endl;
    this->last = NULL;
    this->first = NULL;
    this->length = 0;
}

Queue::~Queue()
{
    cout << "Queue destroy'd" << endl;
}

void Queue::set_first(Link *new_first)
{
    this->first = new_first;
}

void Queue::set_last(Link *new_last)
{
    this->last = new_last;
}

void Queue::set_length(unsigned int new_length)
{
    this->length = new_length;
}

Link *Queue::get_first()
{
    return this->first;
}

Link *Queue::get_last()
{
    return this->last;
}

unsigned int Queue::get_length()
{
    return this->length;
}

void Queue::decrement_length()
{
  this->set_length(this->get_length() - 1);
}

void Queue::increment_length()
{
  this->set_length(this->get_length() + 1);
}

Link *Queue::pop()
{
    if (this->get_length() > 0) {
        Link *aux = this->get_first();
        Link *new_first = aux->prev;
        this->set_first(new_first);
        new_first->next = NULL;

        delete aux;
        this->decrement_length();
    } else {
        cout << "Empty Queue!" << endl;
    }
}

void Queue::push(int new_value)
{
  Link *aux = new Link;
  if (this->get_length() > 0) {
    aux->value = new_value;
    aux->prev = NULL;
    aux->next = this->get_last();
    this->get_last()->prev = aux;
    this->set_last(aux);
    this->increment_length();
  } else {
    aux->next = NULL;
    aux->prev = NULL;
    aux->value = new_value;

    this->set_first(aux);
    this->set_last(aux);
    this->set_length(1);
  }
}

void Queue::show()
{
  if (this->get_length() > 0) {
    Link *aux = this->get_last();
    for (unsigned int i=0; i < this->get_length() - 1; i++) {
      cout << aux->value << endl;
      aux = aux->next;
    }
    cout << aux->value << endl;
  } else {
    cout << "Empty queue." << endl;
  }
}
