class List
{
private:
  Link *first;
  Link *last;
  unsigned int length;
public:
  /* constructors and destructors */
  List();
  ~List();

  /* getters and setters */
  void set_length(unsigned int new_length);
  unsigned int get_length();

  void set_first(Link *new_first);
  Link *get_first();

  void set_last(Link *new_last);
  Link *get_last();

  /* others methods */
  void decrement_length();
  void increment_length();
  void show();
  Link *seek_for(int key);
  void insert_value_at(int key, int new_value, int option);
  void insert_after_value(int new_value, Link *aux);
  void insert_before_value(int new_value, Link *aux);
  void define_first();
  void define_last();
};