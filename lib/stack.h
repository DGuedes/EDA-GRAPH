class Stack
{
private:
  Node *top;
  Node *bot;
  unsigned int length;
public:
  /* constructors and destructors */
  Stack();
  ~Stack();

  /* getters and setters */
  void set_length(unsigned int new_length);
  unsigned int get_length();

  void set_top(Node *new_top);
  Node *get_top();

  void set_bot(Node *new_bot);
  Node *get_bot();

  /* others methods */
  Node *pop();
  void decrement_length();
  void push(int new_value);
  void increment_length();
  void show();

};