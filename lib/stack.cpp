#ifndef Stack
#include "stack.h"
#endif

#include <iostream>

using namespace std;

Stack::Stack()
{
  this->set_length(0);
  this->top = NULL;
  this->bot = NULL;
  cout << "Stack created." << endl;
}

Stack::~Stack()
{
  cout << "Stack destroy'd" << endl;
}

void Stack::set_length(unsigned int new_length)
{
  this->length = new_length;
}

unsigned int Stack::get_length()
{
  return this->length;
}

void Stack::set_top(Node *new_top)
{
  this->top = new_top;
}

Node *Stack::get_top()
{
  return this->top;
}

void Stack::set_bot(Node *new_bot)
{
  this->bot = new_bot;
}

Node *Stack::get_bot()
{
  return this->bot;
}

void Stack::increment_length()
{
  this->set_length(this->get_length() + 1);
}

void Stack::decrement_length()
{
  this->set_length(this->get_length() - 1);
}

Node *Stack::pop()
{
  if (this->get_length() > 0) {
    Node *at_bot = this->get_bot();
    if (this->get_length() == 1) {
      this->top = NULL;
      this->bot = NULL;
      this->set_length(0);
    } else {
      for (unsigned int i=0; i < this->get_length() - 2; i++)
        at_bot = at_bot->above;
      this->set_top(at_bot);
      at_bot = at_bot->above;
      this->get_top()->above = NULL;
      this->decrement_length();
    }
    cout << "TAMANHO: " << this->get_length() << endl;
    return at_bot;
  } else {
    cout << "Empty stack." << endl;
  }
}

void Stack::push(int new_value)
{
  Node *aux = new Node;
  aux->value = new_value;
  aux->above = NULL;
  if (this->get_length() > 0) {
    Node *previous = this->get_top();
    previous->above = aux;
    this->set_top(aux);
    this->increment_length();

  } else {
    this->set_top(aux);
    this->set_bot(aux);
    this->set_length(1);
  }
}

void Stack::show()
{
  if (this->get_length() > 0) {
    Node *aux = this->get_bot();
    for (unsigned int i=0; i < this->get_length() - 1; i++) {
      cout << aux->value << endl;
      aux = aux->above;
    }
    cout << aux->value << endl;
  } else {
    cout << "Empty queue." << endl;
  }
}
