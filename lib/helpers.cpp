#include <iostream>

using namespace std;

#ifndef HIGHEST
#define HIGHEST(a, b)(a > b ? a : b)
#endif

#ifndef Node
struct Node
{
  int value;
  Node *above;
};
#endif

#ifndef Link
struct Link
{
  int value;
  Link *prev;
  Link *next;
  string name;
};
#endif
