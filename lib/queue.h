#ifndef Queue
class Queue
{
private:
  Link *first;
  Link *last;
  unsigned int length;
public:
  /* constructors and destructors */
  Queue();
  ~Queue();

  /* getters and setters */
  void set_length(unsigned int new_length);
  unsigned int get_length();

  void set_first(Link *new_first);
  Link *get_first();

  void set_last(Link *new_last);
  Link *get_last();

  /* others methods */
  Link *pop();
  void decrement_length();
  void push(int new_value);
  void increment_length();
  void show();
};
#endif
