#ifndef List
#include "list.h"
#endif

#include <iostream>

using namespace std;

List::List()
{
  cout << "List created." << endl;
  this->last = NULL;
  this->first = NULL;
  this->length = 0;
}

List::~List()
{
  cout << "List destroy'd" << endl;
}

void List::set_first(Link *new_first)
{
  this->first = new_first;
}

void List::set_last(Link *new_last)
{
  this->last = new_last;
}

void List::set_length(unsigned int new_length)
{
  this->length = new_length;
}

Link *List::get_first()
{
  return this->first;
}

Link *List::get_last()
{
  return this->last;
}

unsigned int List::get_length()
{
  return this->length;
}

void List::decrement_length()
{
  this->set_length(this->get_length() - 1);
}

void List::increment_length()
{
  this->set_length(this->get_length() + 1);
}

void List::show()
{
  if (this->get_length() > 0) {
    Link *aux = this->get_last();
    for (unsigned int i=0; i < this->get_length() - 1; i++) {
      cout << aux->value << endl;
      aux = aux->next;
    }
    cout << aux->value << endl;
  } else {
    cout << "Empty list." << endl;
  }
}

void List::insert_value_at(int key, int new_value, int option)
{
  if (this->get_length() > 0) {
    Link *aux = seek_for(key);
    if (aux != NULL) {
      switch (option) {
      case 1 :
        insert_before_value(new_value, aux);
      break;
      case 2 :
        insert_after_value(new_value, aux);
      break;

      default:
        cout << "Invalid value" << endl;
      }
    } else {
      cout << "Value not found." << endl;
    }
  } else {
    Link *aux = new Link;
    aux->value = new_value;
    aux->next = NULL;
    aux->prev = NULL;

    this->set_first(aux);
    this->set_last(aux);
    this->set_length(1);
  }
}

void List::insert_after_value(int new_value, Link *aux)
{
  Link *new_aux = new Link;

  new_aux->next = NULL;
  new_aux->prev = NULL;
  new_aux->value = new_value;

  if (this->get_length() > 1 && aux != this->get_first()) {
    Link *other = aux->next;

    aux->next = new_aux;
    new_aux->prev = aux;

    other->prev = new_aux;
    new_aux->next = other;

    this->define_first();
  } else {
    new_aux->prev = aux;
    aux->next = new_aux;

    this->define_first();
  }

  this->increment_length();
}

void List::define_last()
{
  if (this->get_length() > 0) {
    Link *aux = this->get_first();
    while (aux->prev != NULL) {
      aux = aux->prev;
    }
    this->set_last(aux);
  } else {
    cout << "Empty List" << endl;
  }
}

void List::define_first()
{
  if (this->get_length() > 0) {
    Link *aux = this->get_last();
    while (aux->next != NULL) {
      aux = aux->next;
    }
    this->set_first(aux);
  } else {
    cout << "Empty list" << endl;
  }
}

void List::insert_before_value(int new_value, Link *aux)
{
  Link *new_aux = new Link;

  new_aux->next = NULL;
  new_aux->prev = NULL;
  new_aux->value = new_value;

  if (this->get_length() > 1 && aux != this->get_last()) {
    Link *other = aux->prev;

    aux->prev = new_aux;
    new_aux->next = aux;

    other->next = new_aux;
    new_aux->prev = other;

    this->define_last();
  } else {
    new_aux->next = aux;
    aux->prev = new_aux;

    this->define_last();
  }

  this->increment_length();
}

Link *List::seek_for(int key)
{
  if (this->get_length() > 0) {
    Link *aux = this->get_last();
    while (aux->next != NULL && aux->value != key) {
      aux = aux->next;
    }
    if (aux->value == key) {
      cout << "Value " << key << " found." << endl;
      return aux;
    } else {
      cout << "Value " << key << " not found." << endl;
    }
  } else {
    cout << "Empty list." << endl;
  }
  return NULL;
}
