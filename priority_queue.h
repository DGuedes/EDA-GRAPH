#ifndef PriorityQueue
class PriorityQueue
{
private:
  Way *first;
  Way *last;
  int length;
  Way *found;

public:
  void flush();

  Way *return_best();

  void set_found(Way *new_found);
  Way *get_found();

  bool should_have(Way *example);

  void set_first(Way *new_first);
  Way *get_first();

  void set_last(Way *new_last);
  Way *get_last();

  void set_length(int new_length);
  int get_length();

  void push(Way *w);
  PriorityQueue();

  void increment_length();
  void show();

  void pop();
  void decrement_length();

  bool check_best(City *destination_c, int new_distance);
  bool is_present(City *future_destination);

};
#endif