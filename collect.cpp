#include <iostream>
#include <fstream>
#include "string.h"

using namespace std;

#ifndef Collect
#include "collect.h"
#endif

Collect::Collect()
{
  cout << "Collect created" << endl;
  this->length = 0;
  this->first = NULL;
  this->last = NULL;
}

Collect::~Collect()
{
  cout << "Collect destroy'd" << endl;
}

void Collect::collect_city(City *c, char *abrev)
{
  // string aux_string;
  // unsigned int aux_unsigned_int;

  // char aux_char[60];
  // strcpy(aux_char, "database/");

  // strcat(aux_char, abrev);

  // strcat(aux_char, ".txt");


  // fstream aux_stream(aux_char);

  // aux_stream >> aux_string;
  // c->name = aux_string;
  // aux_stream >> aux_string;
  // aux_stream >> aux_string;
  // c->code = aux_string;
  // aux_stream >> aux_string;
  // aux_stream >> aux_unsigned_int;

  // c->possible_paths = aux_unsigned_int;

  // aux_stream.close();
}

void Collect::set_first(City *new_first)
{
  this->first = new_first;
}

City *Collect::get_first()
{
  return this->first;
}

void Collect::set_last(City *new_last)
{
  this->last = new_last;
}

City *Collect::get_last()
{
  return this->last;
}

void Collect::set_length(unsigned int new_length)
{
  this->length = new_length;
}

unsigned int Collect::get_length()
{
  return this->length;
}

void Collect::show()
{
  // if (this->get_length() > 0) {
  //   City *aux = this->get_first();
  //   for (unsigned int i=0; i < this->get_length() - 1; i++) {
  //     cout << aux->get_name() << "  " << aux->get_code() << "  " << aux->get_possible_paths() << endl;
  //     aux = aux->get_prev();
  //   }
  //   cout << aux->name << "  " << aux->code << "  " << aux->possible_paths << endl;
  // } else {
  //   cout << "EMPTY COLLECT" << endl;
  // }
}

void Collect::increment_length()
{
  this->set_length(this->get_length() + 1);
}

void Collect::push(City *new_last)
{
  if (this->get_length() > 0) {
    this->get_last()->set_prev(new_last);
    new_last->set_next(this->get_last());
    this->increment_length();
    this->set_last(new_last);
  } else {
    this->set_last(new_last);
    this->set_first(new_last);
    this->set_length(1);
  }
}

void Collect::garbage_collector()
{
  // if (this->get_length() > 0) {
  //   City *aux = this->get_first();
  //   City *other;
  //   while (aux->prev != NULL) {
  //     other = aux->prev;
  //     delete aux;
  //     aux = other;
  //   }
  //   delete aux;
  // }
}

City *Collect::search_by_name(string const origin)
{
  if (this->get_length() > 1) {
    City *aux = this->get_first();
    while (aux->get_prev() != NULL && aux->get_name() != origin) {
      aux = aux->get_prev();
    }
    if (aux->get_name() == origin)
      return aux;
    else
      return NULL;
  } else {
    cout << "Empty collector" << endl;
  }
}

void Collect::best_first_search(string const origin, string const destination)
{
  City *a = this->search_by_name(origin);
  City *b = this->search_by_name(destination);
  if (a != NULL && b != NULL) {
    this->search_solution(a, b);
  } else {
    cout << "NAO EXISTEM" << endl;
  }
}

void Collect::search_solution(City *future_origin, City *future_destination)
{
  Way w;
  w.set_length(0);
  string solution;

  for (unsigned int i=0; i < future_origin->get_possible_paths(); i++) {
    Path *aux = future_origin->get_first_possible_path();
    if (aux->get_destination() == future_destination) {
      cout << "Solution: " << aux->get_name() << endl;
      cout << "Distance: " << aux->get_distance() << endl;
    }
  }

  // Way w;
  // pp->origin = future_origin;
  // pp->destination = future_destination;

  // Path *aux = future_origin->path_controller->first;
  // Path *prev_aux;

  // while (aux->destination != future_destination) {

  //   prev_aux = aux;
  //   cout << "um push" << endl;

  //   aux = aux->destination->path_controller->first;

  //   w.push(prev_aux);
  // }
}

void Collect::popule_path(City *c, char *abrev)
{

  // PathController *controller = new PathController;
  // c->path_controller = controller;
  // char aux[50];
  // strcpy(aux, "database/");
  // strcat(aux, abrev);
  // strcat(aux, "_paths.txt");
  // fstream aux_database(aux);
  // string aux_code;
  // string aux_origem;
  // string aux_destino;
  // int aux_distance;
  // string aux_string;

  // for (unsigned int i=0; i < c->possible_paths; i++) {
  //   aux_database >> aux_code;
  //   aux_database >> aux_string;
  //   aux_database >> aux_origem;
  //   aux_database >> aux_string;
  //   aux_database >> aux_destino;
  //   aux_database >> aux_string;
  //   aux_database >> aux_distance;

  //   Path *p = new Path;

  //   p->name = aux_code;
  //   p->distance = aux_distance;
  //   p->visited = true;

  //   c->path_controller->push_path(p);
  // }
}

void Collect::popule()
{
  char aux[3];

  string aux_name;
  string aux_code;
  unsigned int aux_paths;

  City *c01 = new City;
  strcpy(aux, "rs");
  collect_city(c01, aux);
  popule_path(c01, aux);
  this->push(c01);

  City *c02 = new City;
  strcpy(aux, "sc");
  collect_city(c02, aux);
  this->push(c02);

  City *c03 = new City;
  strcpy(aux, "pr");
  collect_city(c03, aux);
  this->push(c03);

  City *c04 = new City;
  strcpy(aux, "sp");
  collect_city(c04, aux);
  this->push(c04);

  City *c05 = new City;
  strcpy(aux, "ms");
  collect_city(c05, aux);
  this->push(c05);

  City *c06 = new City;
  strcpy(aux, "rj");
  collect_city(c06, aux);
  this->push(c06);

  City *c07 = new City;
  strcpy(aux, "mg");
  collect_city(c07, aux);
  this->push(c07);

  City *c08 = new City;
  strcpy(aux, "go");
  collect_city(c08, aux);
  this->push(c08);
}

// void Collect::popule(Graph *g)
// {

// }