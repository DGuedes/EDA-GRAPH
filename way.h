#ifndef Way
class Way
{
private:
  Path *start;
  Path *finish;
  int length;
  int weigth;
  Way *prev;
  Way *next;
  string name;
  City *origin;
  City *destination;
public:
  void show_each_path();

  Path *get_clear_path(City *example);

  void set_name(string new_name);
  string get_name();

  void set_start(Path *new_start);
  Path *get_start();

  void set_finish(Path *new_finish);
  Path *get_finish();

  void set_length(int new_length);
  int get_length();

  void set_weigth(int new_weigth);
  int get_weigth();

  void show();

  void to_s();

  bool has(string future_path);

  Way();
  Way(Way *to_copy);

  void push(Path *p);

  void set_prev(Way *new_prev);
  Way *get_prev();

  void set_next(Way *new_next);
  Way *get_next();

  void set_origin(City *new_origin);
  City *get_origin();

  void set_destination(City *new_destination);
  City *get_destination();

};
#endif