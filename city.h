#include <iostream>

using namespace std;

class Path;

class City
{
private:
  string name;
  unsigned int possible_paths;
  string code;
  City *next;
  City *prev;
  Path *first_possible_path;
  Path *last_possible_path;
  bool visited;

public:
  void set_visited(bool new_visited);
  bool get_visited();

  void set_last_possible_path(Path *new_last);
  Path *get_last_possible_path();

  void set_first_possible_path(Path *new_first);
  Path *get_first_possible_path();

  void set_name(string new_name);
  string get_name();

  void set_possible_paths(unsigned int new_possible_paths);
  unsigned int get_possible_paths();

  void set_code(string new_code);
  string get_code();

  void set_next(City *new_next);
  City *get_next();

  void set_prev(City *new_prev);
  City *get_prev();

  City();
  ~City();

  void increment_possible_paths();
  void push_path(Path *p);
  void print_possible_paths();
  Path *get_path_except(Path *p);
};