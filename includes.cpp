#include "lib/helpers.cpp"

#ifndef Queue
#include "lib/queue.cpp"
#endif



#ifndef List
#include "lib/list.cpp"
#endif

#ifndef Stack
#include "lib/stack.cpp"
#endif

#ifndef Path
#include "path.cpp"
#endif

#ifndef City
#include "city.cpp"
#endif

#ifndef Way
#include "way.cpp"
#endif

#ifndef PriorityQueue
#include "priority_queue.cpp"
#endif

#ifndef Collect
#include "collect.cpp"
#endif

#ifndef Graph
#include "graph.cpp"
#endif

