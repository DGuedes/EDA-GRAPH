#ifndef City
#include "city.h"
#endif

// #ifndef Path
// #include "path.h"
// #endif

#include <iostream>

using namespace std;

void City::set_visited(bool new_visited)
{
  this->visited = new_visited;
}

bool City::get_visited()
{
  return this->visited;
}

Path *City::get_path_except(Path *p)
{
  if (this->get_possible_paths() > 0) {
    Path *aux = this->get_first_possible_path();
    for (unsigned int i=0; i < this->get_possible_paths(); i++) {
      if (aux->get_destination()->get_name() != p->get_origin()->get_name()) {
        return aux;
      }
      aux = aux->get_prev();
    }
  }
}

void City::set_name(string new_name)
{
  this->name = new_name;
}

string City::get_name()
{
  return this->name;
}

void City::set_possible_paths(unsigned int new_possible_paths)
{
  this->possible_paths = new_possible_paths;
}

unsigned int City::get_possible_paths()
{
  return this->possible_paths;
}

City::City()
{
  this->possible_paths = 0;
  this->name = "Invalid";
  this->code = "CXXX";
  this->visited = false;
  // cout << "City created." << endl;
}

City::~City()
{
  // cout << "City destroy'd" << endl;
}

void City::set_code(string new_code)
{
  this->code = new_code;
}

string City::get_code()
{
  return this->code;
}

void City::set_next(City *new_next)
{
  this->next = new_next;
}

City *City::get_next()
{
  return this->next;
}

void City::set_prev(City *new_prev)
{
  this->prev = new_prev;
}

City *City::get_prev()
{
  return this->prev;
}

void City::set_first_possible_path(Path *new_first)
{
  this->first_possible_path = new_first;
}

Path *City::get_first_possible_path()
{
  return this->first_possible_path;
}

void City::print_possible_paths()
{
  if (this->get_possible_paths() > 0) {
    Path *aux = this->get_first_possible_path();
    cout << aux->get_name() << endl;
    cout << aux->get_origin()->get_name() << endl;
    cout << aux->get_destination()->get_name() << endl;

    for (unsigned int i=0; i < this->get_possible_paths() - 1; i++) {
      cout << aux->get_name() << " " << aux->get_origin()->get_name() << " " << aux->get_destination()->get_name() << endl;

      aux = aux->get_prev();

    }
    cout << aux->get_name() << " " << aux->get_origin()->get_name() << " " << aux->get_destination()->get_name() << endl;
  }
}

void City::push_path(Path *p)
{
  if (this->get_first_possible_path() != NULL) {
    this->get_last_possible_path()->prev = p;
    p->next = this->get_last_possible_path();
    this->set_last_possible_path(p);
  } else {
    this->set_last_possible_path(p);
    this->set_first_possible_path(p);
  }
}

void City::increment_possible_paths()
{
  this->set_possible_paths(this->get_possible_paths() + 1);
}

void City::set_last_possible_path(Path *new_last)
{
  this->last_possible_path = new_last;
}

Path *City::get_last_possible_path()
{
  return this->last_possible_path;
}
