class City;
class Path
{
private:
  City *origin;
  City *destination;
  int distance;
  string name;


public:
  Path(Path *example);

  Path *prev;
  Path *next;

  void set_next(Path *new_next);
  Path *get_next();

  void set_prev(Path *new_prev);
  Path *get_prev();

  void set_origin(City *new_origin);
  City *get_origin();

  void set_destination(City *new_destination);
  City *get_destination();

  void set_name(string new_name);
  string get_name();

  void set_distance(int new_distance);
  int get_distance();

  void to_s();

  Path();
  ~Path();
};
