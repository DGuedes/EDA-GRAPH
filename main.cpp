#include <iostream>
#include <fstream>

using namespace std;

#include "path.h"
#include "city.h"

// #include "includes.cpp"

#include "lib/helpers.cpp"
#include "lib/stack.h"
#include "lib/queue.h"
#include "lib/list.h"

#include "way.h"
#include "collect.h"
#include "priority_queue.h"
#include "graph.h"

#include <math.h>
#include <time.h>


int main(int argc, char const *argv[])
{
  PriorityQueue prioridades;
  PriorityQueue historico;
  PriorityQueue solucoes;

  clock_t start_time, end_time;

  Graph g;

  City *c_iterator = g.get_first_city();
  Path *p_iterator = c_iterator->get_first_possible_path();
  for (unsigned int i=0; i < 27; i++) {
    cout << c_iterator->get_name() << " " << c_iterator->get_possible_paths() << endl;
    p_iterator = c_iterator->get_first_possible_path();
    for (unsigned int j=0; j < c_iterator->get_possible_paths() - 1; j++) {
      cout << p_iterator->get_origin()->get_name() << " -> " << p_iterator->get_destination()->get_name() << endl;
      p_iterator = p_iterator->get_prev();
    }
    cout << p_iterator->get_origin()->get_name() << " -> " << p_iterator->get_destination()->get_name() << endl;
    c_iterator = c_iterator->get_prev();
  }

  string aux_string;
  City *origin_aux;
  City *destino_aux;
  string aux_destino;
  std::cout << "Origem: " << std::endl;
  std::cin >> aux_string;
  std::cout << "Destino: " << std::endl;
  std::cin >> aux_destino;

  start_time = clock();

  origin_aux = g.seek_for_city(aux_string);
  destino_aux = g.seek_for_city(aux_destino);

  origin_aux->set_visited(true);

  while (origin_aux == NULL || destino_aux == NULL) {
    std::cout << "Origem ou destino invalidos, insira novamente." << std::endl;
    std::cout << "Origem: " << std::endl;
    std::cin >> aux_string;
    std::cout << "Destino: " << std::endl;
    std::cin >> aux_destino;
    origin_aux = g.seek_for_city(aux_string);
    destino_aux = g.seek_for_city(aux_destino);
  }

  Path *p_aux_teste;
  Path *store_path = origin_aux->get_first_possible_path();
  Path *to_store_path;
  Way *aux;
  Way *w;

  for (unsigned int i=0; i < origin_aux->get_possible_paths(); i++) {
    if (store_path->get_destination()->get_name() == aux_destino) {
      cout << "to aqui" << endl;
      cout << "LENGTH: " << prioridades.get_length() << endl;
      for (unsigned int i=0; i < prioridades.get_length(); i++) {
        cout << "aqui tbm" << endl;
        prioridades.pop();
      }
      cout << "depis" << endl;
      w = new Way;
      to_store_path = new Path(store_path);
      w->push(to_store_path);
      solucoes.push(w);
    } else {
      w = new Way;
      to_store_path = new Path(store_path);
      w->push(to_store_path);
      w->show_each_path();
      w->get_destination()->set_visited(true);
      store_path = store_path->get_prev();
      prioridades.push(w);
    }
  }

  aux = prioridades.get_first();
  for (unsigned int i=0; i < prioridades.get_length(); i++) {
    aux->show_each_path();
    aux = aux->get_prev();
  }

  store_path = origin_aux->get_first_possible_path();

  cout << "PRIMEIROS PUSHS: " << endl;
  prioridades.show();
  cout << "****\n\n\n\n\n\n" << endl;

  aux = prioridades.get_first();

  int k=0;

  City *destination_shortcut;
  while (prioridades.get_length() > 0) {
    k++;
    if (k > 40) {
      break;
    }
    prioridades.show();
    aux = prioridades.get_first();
    aux->show_each_path();
    store_path = aux->get_destination()->get_first_possible_path();
    cout << "FIRST STORE PATH:" << store_path->get_origin()->get_name() << " -> " << store_path->get_destination()->get_name() << endl;
    for (unsigned int i=0; i < aux->get_destination()->get_possible_paths(); i++) {
      to_store_path = new Path(store_path);
      cout << store_path->get_destination()->get_name() << " = " << destino_aux->get_name() << "  ?" << endl;
      if (store_path->get_destination()->get_name() == destino_aux->get_name()) {
        aux = new Way(aux);
        aux->push(to_store_path);
        solucoes.push(aux);
        break;
      } else {
        if (store_path->get_destination()->get_visited() != true) {
          aux->show_each_path();
          aux = new Way(aux);

          aux->push(to_store_path);
          aux->get_destination()->set_visited(true);
          prioridades.push(aux);
          cout << "PUSH: " << store_path->get_origin()->get_name() << " -> " << store_path->get_destination()->get_name() << endl;
          // cout << "PRIORIDADES: " << endl;
          // prioridades.show();
          // cout << "\n\n\n\n\n\n\n\n\n\n\n" << endl;
        } else {
          cout << store_path->get_destination()->get_name() << "visitado" << endl;
        }
      }
      store_path = store_path->prev;
      aux = prioridades.get_first();
    }
    prioridades.pop();
    cout << "prioridades: " << endl;
    cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n LOOP FEITO" << endl;
  }

  cout << "SOLUCOES \n\n\n\n\n\n\n" << endl;

  solucoes.show();

  cout << "melhor soluçao: " << endl;
  cout << solucoes.return_best()->get_name() << endl;
  Way *teste = solucoes.return_best();
  teste->show_each_path();

  end_time = clock();

  double time_in_seconds = (clock() - start_time) / (double)CLOCKS_PER_SEC;

  cout << "O tempo de execucao em segundos: " << time_in_seconds << endl;

  delete aux;
  delete w;
  delete to_store_path;

  return 0;
}
