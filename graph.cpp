#ifndef Graph
#include "graph.h"
#endif

#include <fstream>
#include <iostream>

using namespace std;

void Graph::fetch_path(City *city, char const *abrev)
{
  string aux_string;
  int aux_int;
  char aux[60];

  strcpy(aux, "database/");
  strcat(aux, abrev);
  strcat(aux, "_paths.txt");
  fstream aux_stream(aux);

  for (unsigned int i=0; i < city->get_possible_paths(); i++) {
    Path *new_path = new Path;
    aux_stream >> aux_string; // nome do path
    new_path->set_name(aux_string);
    aux_stream >> aux_string; // barra
    aux_stream >> aux_string; // origem
    new_path->set_origin(city);
    aux_stream >> aux_string; // barra
    aux_stream >> aux_string; // destino
    this->seek_for_city(aux_string);
    City *aux_city = this->seek_for_city(aux_string);
    new_path->set_destination(aux_city);
    aux_stream >> aux_string; // barra
    aux_stream >> aux_int; // distancia
    new_path->set_distance(aux_int);


    city->push_path(new_path);
  }
  aux_stream.close();
}

Graph::Graph()
{
  cout << "Graph created." << endl;
  this->set_length(0);
  City *cities = new City[28];
  this->fetch(&cities[0], "rs");
  this->fetch(&cities[1], "sc");
  this->fetch(&cities[2], "pr");
  this->fetch(&cities[3], "sp");
  this->fetch(&cities[4], "ms");
  this->fetch(&cities[5], "rj");
  this->fetch(&cities[6], "mg");
  this->fetch(&cities[7], "go");
  this->fetch(&cities[8], "mt");
  this->fetch(&cities[9], "es");
  this->fetch(&cities[10], "ba");
  this->fetch(&cities[11], "df");
  this->fetch(&cities[12], "to");
  this->fetch(&cities[13], "pa");
  this->fetch(&cities[14], "ro");
  this->fetch(&cities[15], "am");
  this->fetch(&cities[16], "se");
  this->fetch(&cities[17], "al");
  this->fetch(&cities[18], "pe");
  this->fetch(&cities[19], "pi");
  this->fetch(&cities[20], "ma");
  this->fetch(&cities[21], "ap");
  this->fetch(&cities[22], "rr");
  this->fetch(&cities[23], "ac");
  this->fetch(&cities[24], "pb");
  this->fetch(&cities[25], "ce");
  this->fetch(&cities[26], "rn");

  this->push_city(&cities[0]);
  this->push_city(&cities[1]);
  this->push_city(&cities[2]);
  this->push_city(&cities[3]);
  this->push_city(&cities[4]);
  this->push_city(&cities[5]);
  this->push_city(&cities[6]);
  this->push_city(&cities[7]);
  this->push_city(&cities[8]);
  this->push_city(&cities[9]);
  this->push_city(&cities[10]);
  this->push_city(&cities[11]);
  this->push_city(&cities[12]);
  this->push_city(&cities[13]);
  this->push_city(&cities[14]);
  this->push_city(&cities[15]);
  this->push_city(&cities[16]);
  this->push_city(&cities[17]);
  this->push_city(&cities[18]);
  this->push_city(&cities[19]);
  this->push_city(&cities[20]);
  this->push_city(&cities[21]);
  this->push_city(&cities[22]);
  this->push_city(&cities[23]);
  this->push_city(&cities[24]);
  this->push_city(&cities[25]);
  this->push_city(&cities[26]);

  this->fetch_path(&cities[0], "rs");
  this->fetch_path(&cities[1], "sc");
  this->fetch_path(&cities[2], "pr");
  this->fetch_path(&cities[3], "sp");
  this->fetch_path(&cities[4], "ms");
  this->fetch_path(&cities[5], "rj");
  this->fetch_path(&cities[6], "mg");
  this->fetch_path(&cities[7], "go");
  this->fetch_path(&cities[8], "mt");
  this->fetch_path(&cities[9], "es");
  this->fetch_path(&cities[10], "ba");
  this->fetch_path(&cities[11], "df");
  this->fetch_path(&cities[12], "to");
  this->fetch_path(&cities[13], "pa");
  this->fetch_path(&cities[14], "ro");
  this->fetch_path(&cities[15], "am");
  this->fetch_path(&cities[16], "se");
  this->fetch_path(&cities[17], "al");
  this->fetch_path(&cities[18], "pe");
  this->fetch_path(&cities[19], "pi");
  this->fetch_path(&cities[20], "ma");
  this->fetch_path(&cities[21], "pa");
  this->fetch_path(&cities[22], "rr");
  this->fetch_path(&cities[23], "ac");
  this->fetch_path(&cities[24], "pb");
  this->fetch_path(&cities[25], "ce");
  this->fetch_path(&cities[26], "rn");
  // this->cities = NULL;
}

Graph::~Graph()
{
  cout << "Graph destroy'd" << endl;
}

unsigned int Graph::get_length()
{
  return this->length;
}

void Graph::set_length(unsigned int new_length)
{
  this->length = new_length;
}

void Graph::set_first_city(City *new_first_city)
{
  this->first_city = new_first_city;
}

City *Graph::get_first_city()
{
  return this->first_city;
}

void Graph::set_last_city(City *new_last_city)
{
  this->last_city = new_last_city;
}

City *Graph::get_last_city()
{
  return this->last_city;
}

void Graph::push_city(City *new_city)
{
  if (this->get_length() > 0) {
    this->get_last_city()->set_prev(new_city);
    new_city->set_next(this->get_last_city());
    this->set_last_city(new_city);

    this->increment_length();
  } else {
    this->set_first_city(new_city);
    this->set_last_city(new_city);
    this->set_length(1);
  }
}

void Graph::increment_length()
{
  this->set_length(this->get_length() + 1);
}

// void Graph::set_cities(City *new_cities)
// {
//   this->cities = new_cities;
// }

// City *Graph::get_cities()
// {
//   return this->cities;
// }

void Graph::display_cities()
{
  if (this->get_length() > 1) {
    City *aux = this->get_first_city();
    cout << aux->get_name() << endl;
    for (unsigned int i=0; i < this->get_length() - 1; i++) {
      cout << aux->get_name() << " " << aux->get_code() << " " << aux->get_possible_paths() << endl;
      aux = aux->get_prev();
    }
    cout << aux->get_name() << " " << aux->get_code() << " " << aux->get_possible_paths() << endl;
  }
}

void Graph::fetch(City *c, const char *abrev)
{
  string aux_string;
  unsigned int aux_unsigned_int;
  char aux_char[60];

  strcpy(aux_char, "database/");
  strcat(aux_char, abrev);
  strcat(aux_char, ".txt");

  fstream aux_stream(aux_char);

  aux_stream >> aux_string;
  c->set_name(aux_string);
  aux_stream >> aux_string;
  aux_stream >> aux_string;
  c->set_code(aux_string);
  aux_stream >> aux_string;
  aux_stream >> aux_unsigned_int;

  c->set_possible_paths(aux_unsigned_int);
  aux_stream.close();
}

City *Graph::seek_for_city(string const name)
{
  if (this->get_length() > 1) {
    City *aux = this->get_first_city();
    for (unsigned int i=0; i < this->get_length(); i++) {
      if (aux->get_name() == name)
        break;
      aux = aux->get_prev();
    }
    if (aux->get_name() == name) {
      return aux;
    } else {
      cout << "City not found" << endl;
      return NULL;
    }
  } else {
    cout << "Empty Graph" << endl;
  }
}

void Graph::get_fetch()
{
  City cities[28];
  this->fetch(&cities[0], "ac");
  this->fetch(&cities[1], "al");
  this->fetch(&cities[2], "am");
  this->fetch(&cities[3], "ap");
  this->fetch(&cities[4], "ba");
  this->fetch(&cities[5], "ce");
  this->fetch(&cities[6], "df");
  this->fetch(&cities[7], "es");
  this->fetch(&cities[8], "go");
  this->fetch(&cities[9], "ma");
  this->fetch(&cities[10], "mg");
  this->fetch(&cities[11], "ms");
  this->fetch(&cities[12], "mt");
  this->fetch(&cities[13], "pa");
  this->fetch(&cities[14], "pb");
  this->fetch(&cities[15], "pe");
  this->fetch(&cities[16], "pi");
  this->fetch(&cities[17], "pr");
  this->fetch(&cities[19], "rj");
  this->fetch(&cities[20], "rn");
  this->fetch(&cities[21], "ro");
  this->fetch(&cities[22], "rr");
  this->fetch(&cities[23], "rs");
  this->fetch(&cities[24], "sc");
  this->fetch(&cities[25], "se");
  this->fetch(&cities[26], "sp");

  this->push_city(&cities[0]);
  this->push_city(&cities[1]);
  this->push_city(&cities[2]);
  this->push_city(&cities[3]);
  this->push_city(&cities[4]);
  this->push_city(&cities[5]);
  this->push_city(&cities[6]);
  this->push_city(&cities[7]);
  this->push_city(&cities[8]);
  this->push_city(&cities[9]);
  this->push_city(&cities[10]);
  this->push_city(&cities[11]);
  this->push_city(&cities[12]);
  this->push_city(&cities[13]);
  this->push_city(&cities[14]);
  this->push_city(&cities[15]);
  this->push_city(&cities[16]);
  this->push_city(&cities[17]);
  this->push_city(&cities[18]);
  this->push_city(&cities[19]);
  this->push_city(&cities[20]);
  this->push_city(&cities[21]);
  this->push_city(&cities[22]);
  this->push_city(&cities[23]);
  this->push_city(&cities[24]);
  this->push_city(&cities[25]);
  this->push_city(&cities[26]);
}